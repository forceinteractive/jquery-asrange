import $ from 'jquery';


export default {
  defaults: {},
  init(instance) {
    this.$arrow = $('<span></span>').appendTo(instance.$wrap);
    this.$arrow.addClass(`${instance.namespace}-selected`);

    if (instance.options.range === false) {
      instance.p1.$element.on(`${instance.namespace}::move`, (e, pointer) => {

        let styles = {
          left: 0,
          width: `${pointer.getPercent()}%`
        };

        if(instance.direction.position === 'top') {
          let percent = 100 - pointer.getPercent();
          styles = {
            bottom: 0,
            height: `${percent}%`
          }
        }

        this.$arrow.css(styles);
      });
    }

    if (instance.options.range === true) {
      const onUpdate = () => {
        let width = instance.p2.getPercent() - instance.p1.getPercent();
        let left;
        if (width >= 0) {
          left = instance.p1.getPercent();
        } else {
          width = -width;
          left = instance.p2.getPercent();
        }

        let styles = {
          left: `${left}%`,
          width: `${width}%`
        };

        if(instance.direction.position === 'top') {
          styles = {
            bottom: `${left}%`,
            height: `${width}%`
          };
        }

        this.$arrow.css(styles);
      };
      instance.p1.$element.on(`${instance.namespace}::move`, onUpdate);
      instance.p2.$element.on(`${instance.namespace}::move`, onUpdate);
    }
  }
};
